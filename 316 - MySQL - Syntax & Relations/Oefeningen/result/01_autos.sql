## 1
CREATE DATABASE IF NOT EXISTS `cmm_autos`;

## 2

USE `cmm_autos`;

DROP TABLE IF EXISTS `autos`;
CREATE TABLE `autos`(
  id INT(11) NOT NULL auto_increment,
  merk_id INT(11), 
  titel VARCHAR(255), 
  type VARCHAR(255), 
  kleur VARCHAR(255), 
  brandstof VARCHAR(255), 
  zitplaatsen INT(11),
  prijs FLOAT(8),
  PRIMARY KEY (id)
);

SHOW COLUMNS FROM autos;

## 3

DROP TABLE IF EXISTS `merken`;
CREATE TABLE `merken`(
  id INT(11) NOT NULL auto_increment,
  naam VARCHAR(255), 
  land VARCHAR(255), 
  PRIMARY KEY (id)
);
SHOW COLUMNS FROM merken;

## 4

INSERT INTO autos (merk_id, titel, type, kleur, brandstof, zitplaatsen, prijs) VALUES
(1, 'Mustang', 'Sport', 'Rood', 'Benzine', 2, 40000),
(1, 'Focus', 'Station', 'Blauw', 'Diesel', 5, 37000),
(2, 'Polo', 'Cabrio', 'Geel', 'Benzine', 4, 19999.99),
(3, 'Clio', 'Luxe', 'Rood', 'Benzine', 4, 23450.12),
(4, '307', 'GT', 'Grijs', 'Benzine', 4, 1234.50),
(6, 'Model-T', 'Sport', 'Rood', 'Electrisch', 2, 80000.00);

INSERT INTO merken (id, naam, land) VALUES
(1, 'Ford',' Amerika'),
(2, 'Volkswagen','Duitsland'),
(3, 'Renault','Frankrijk'),
(4, 'Peugeot','Frankrijk'),
(5, 'Toyota','Japan'),
(6, 'Tesla','Amerika');

## 5

SELECT * FROM autos;

## 6

SELECT * FROM autos
WHERE kleur LIKE '%l%';

## 7

SELECT m.naam, a.titel, m.land 
FROM autos AS a
LEFT JOIN merken AS m
ON a.merk_id = m.id;

## 8

SELECT m.naam, a.titel, m.land 
FROM autos AS a
LEFT JOIN merken AS m
ON a.merk_id = m.id
ORDER BY m.naam, a.titel;

## 9
SELECT m.naam, COUNT(m.naam) AS Aantal 
FROM autos AS a
LEFT JOIN merken AS m
ON a.merk_id = m.id
GROUP BY m.naam
ORDER BY m.naam;

### FOUT ###
## want Toyota krijgt NULL als resultaat, maar DAT IS een resultaat dus 1
SELECT m.naam, COUNT(m.naam) AS Aantal 
FROM autos AS a
RIGHT JOIN merken AS m
ON a.merk_id = m.id
GROUP BY m.naam
ORDER BY m.naam;



