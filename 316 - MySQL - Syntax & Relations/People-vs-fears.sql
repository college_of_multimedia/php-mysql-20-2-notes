DROP DATABASE IF EXISTS `CMM_test`;
CREATE DATABASE IF NOT EXISTS `CMM_test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `CMM_test`;

-- 
-- Table structure for table `people`
-- 

DROP TABLE IF EXISTS `people`;
CREATE TABLE IF NOT EXISTS `people` (
  `PersonID` smallint(5) unsigned NOT NULL auto_increment,
  `Name` varchar(50) default NULL,
  `Sofi` varchar(11) default NULL,
  `Email` varchar(50) default NULL,
  PRIMARY KEY  (`PersonID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


-- 
-- Dumping data for table `people`
-- 

INSERT INTO `people` VALUES (1, 'John Johnson', '123-456-789', NULL);
INSERT INTO `people` VALUES (2, 'Jane Jones', '987-564-123', NULL);
INSERT INTO `people` VALUES (3, 'Aloysius Snuffleupagus', '564-789-321', NULL);
INSERT INTO `people` VALUES (4, 'ik', '123456789', NULL);


--
-- Table structure for table `fears`
--

CREATE TABLE IF NOT EXISTS `fears` (
  `FearID` int(10) NOT NULL auto_increment,
  `Fear` varchar(25) NOT NULL,
  PRIMARY KEY  (`FearID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fears`
--

INSERT INTO `fears` (`FearID`, `Fear`) VALUES
(1, 'Black Cats'),
(2, 'Friday 13th'),
(3, 'Peanut'),
(4, 'Heights'),
(5, 'Flying');

-- --------------------------------------------------------

--
-- Table structure for table `person_fear`
--

CREATE TABLE IF NOT EXISTS `person_fear` (
  `id` int(10) NOT NULL auto_increment,
  `PersonID` int(10) NOT NULL,
  `FearID` int(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_fear`
--

INSERT INTO `person_fear` (`PersonID`, `FearID`) VALUES
(1, 1),
(1, 2),
(1, 5),
(2, 2),
(2, 3),
(2, 5),
(3, 1),
(4, 1),
(3, 2),
(3, 3);


