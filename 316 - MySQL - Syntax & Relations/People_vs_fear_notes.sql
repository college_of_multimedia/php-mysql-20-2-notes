SELECT p.Name, f.fear 
FROM people AS p, fears AS f, person_fear AS pf 
WHERE p.PersonID = pf.PersonID 
AND pf.FearID = f.FearID 
ORDER BY p.Name, f.fear;

SELECT people.Name, fears.fear 
FROM people, fears, person_fear 
WHERE people.PersonID = person_fear.PersonID 
AND person_fear.FearID = fears.FearID 
ORDER BY people.Name, fears.fear;
+------------------------+-------------+
| Name                   | fear        |
+------------------------+-------------+
| Aloysius Snuffleupagus | Black Cats  |
| Aloysius Snuffleupagus | Friday 13th |
| Aloysius Snuffleupagus | Peanut      |
| ik                     | Black Cats  |
| Jane Jones             | Flying      |
| Jane Jones             | Friday 13th |
| Jane Jones             | Peanut      |
| John Johnson           | Black Cats  |
| John Johnson           | Flying      |
| John Johnson           | Friday 13th |
+------------------------+-------------+

SELECT p.Name AS Naam, COUNT(p.Name) AS Angsten 
FROM people AS p, fears AS f, person_fear AS pf 
WHERE p.PersonID = pf.PersonID 
AND pf.FearID = f.FearID 
GROUP BY p.Name 
ORDER BY p.Name;
+------------------------+---------+
| Naam                   | Angsten |
+------------------------+---------+
| Aloysius Snuffleupagus |       3 |
| ik                     |       1 |
| Jane Jones             |       3 |
| John Johnson           |       3 |
+------------------------+---------+

SELECT f.fear AS Angst, COUNT(f.fear) AS Aantal
FROM fears AS f, person_fear AS pf 
WHERE pf.FearID = f.FearID 
GROUP BY Angst
ORDER BY Aantal DESC, Angst
LIMIT 3;
+-------------+--------+
| Angst       | Aantal |
+-------------+--------+
| Black Cats  |      3 |
| Friday 13th |      3 |
| Flying      |      2 |
+-------------+--------+

# JOINS

SELECT Name, FearID
FROM people
LEFT JOIN person_fear
ON people.PersonID = person_fear.PersonID;
+------------------------+--------+
| Name                   | FearID |
+------------------------+--------+
| John Johnson           |      1 |
| John Johnson           |      2 |
| John Johnson           |      5 |
| Jane Jones             |      2 |
| Jane Jones             |      3 |
| Jane Jones             |      5 |
| Aloysius Snuffleupagus |      1 |
| ik                     |      1 |
| Aloysius Snuffleupagus |      2 |
| Aloysius Snuffleupagus |      3 |
+------------------------+--------+

SELECT *
FROM people
LEFT JOIN person_fear
ON people.PersonID = person_fear.PersonID;

SELECT *
FROM people
LEFT JOIN person_fear
ON people.id = person_fear.PersonID;


SELECT *
FROM people
NATURAL JOIN person_fear;
## er moet dus een kolom in beide tabellen met een matchende naam zijn


INSERT INTO fears (Fear) VALUES
('Examns'),
('Clowns');

INSERT INTO `people` (`Name`, `Sofi`) VALUES
('Boris Belts', '545-456-789'),
('Mister T', '987-456-123'),
('Tony Starch', '545-456-789');

INSERT INTO person_fear (PersonID, FearID) VALUES
(6, 1),
(5, 6),
(5, 2),
(5, 5);

SELECT Name, FearID
FROM people
LEFT JOIN person_fear
ON people.PersonID = person_fear.PersonID;
+------------------------+--------+
| Name                   | FearID |
+------------------------+--------+
| John Johnson           |      1 |
| John Johnson           |      2 |
| John Johnson           |      5 |
| Jane Jones             |      2 |
| Jane Jones             |      3 |
| Jane Jones             |      5 |
| Aloysius Snuffleupagus |      1 |
| ik                     |      1 |
| Aloysius Snuffleupagus |      2 |
| Aloysius Snuffleupagus |      3 |
| Mister T               |      1 |
| Boris Belts            |      6 |
| Boris Belts            |      2 |
| Boris Belts            |      5 |
| Tony Starch            |   NULL |
+------------------------+--------+

SELECT Name, FearID
FROM people
RIGHT JOIN person_fear
ON people.PersonID = person_fear.PersonID;
+------------------------+--------+
| Name                   | FearID |
+------------------------+--------+
| John Johnson           |      1 |
| John Johnson           |      2 |
| John Johnson           |      5 |
| Jane Jones             |      2 |
| Jane Jones             |      3 |
| Jane Jones             |      5 |
| Aloysius Snuffleupagus |      1 |
| ik                     |      1 |
| Aloysius Snuffleupagus |      2 |
| Aloysius Snuffleupagus |      3 |
| Mister T               |      1 |
| Boris Belts            |      6 |
| Boris Belts            |      2 |
| Boris Belts            |      5 |
+------------------------+--------+

SELECT f.fear AS Angst, COUNT(f.fear) AS Aantal
FROM fears AS f
LEFT JOIN person_fear AS pf 
ON pf.FearID = f.FearID
GROUP BY Angst
ORDER BY Aantal ASC
LIMIT 3;
+---------+--------+
| Angst   | Aantal |
+---------+--------+
| Examns  |      1 |
| Heights |      1 |
| Clowns  |      1 |
+---------+--------+

SELECT f.fear AS Angst, COUNT(f.fear) AS Aantal
FROM person_fear AS pf
LEFT JOIN fears AS f 
ON pf.FearID = f.FearID
GROUP BY Angst
ORDER BY Aantal ASC
LIMIT 3;


