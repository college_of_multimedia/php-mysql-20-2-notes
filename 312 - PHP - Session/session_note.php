<?php
session_start();


if ( !session_id() ){
	session_start();
}

$_SESSION['username'] = 'Harald';
$_SESSION['login'] = true;

unset($_SESSION['login']);
session_abort();


//print_r( $_SESSION );

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document van <?= $_SESSION['username'] ?></title>
</head>
<body>
	<h1>Homepage van <?= $_SESSION['username'] ?></h1>
	
</body>
</html>