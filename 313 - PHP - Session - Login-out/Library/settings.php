<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);

define('DOCUMENT_ROOT', realpath( $_SERVER['DOCUMENT_ROOT'] ) );
define('SITE_ROOT',     realpath( DOCUMENT_ROOT . '/..'     ) );
define('LIB_ROOT',      realpath( SITE_ROOT . '/Library'    ) );
define('INC',      		realpath( LIB_ROOT . '/Includes'    ) );

define('WEB_URL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);

if ( ! session_id() ){
	session_start();
}

$bgcolor = 'red';

// login script
if ( isset($_POST['username']) && 'harald' == $_POST['username'] && isset( $_POST['password'] ) ) {
	// en in het echt valideren en checken in de database
	$_SESSION['user'] = ucfirst( trim( $_POST['username'] ) );
	$_SESSION['user_id'] = 123;
	$bgcolor = 'green';
	setcookie('bgcolor', 'green', time() + 42000); // month
}

if ( isset( $_COOKIE['bgcolor'] ) ){
	$bgcolor = $_COOKIE['bgcolor'];
}

// logout script
if ( isset( $_GET['logout'] ) ){
	// vergeet je koekie door een houdbaarheidsdatum in het verleden te maken 
	setcookie('bgcolor', '', time() - 42000); // month ago

	// verwijder alle sessie variabelen
	$_SESSION = array();

	// als de sessie ook onthouden wordt door een cookie, zorg dat deze ook verloopt!
	if ( ini_get('session.use_cookies') ){
		$params = session_get_cookie_params();
		setcookie(
			session_name(),
			'', 
			time() - 42000, 
			$params['path'],
			$params['domain'],
			$params['secure'],
			$params['httponly']
		);
	}

	// totaal weg die sessie
	session_destroy();
	
	// navigeer terug naar de agina maar ZONDER ?logout
	header('Location: ' . htmlspecialchars( $_SERVER['PHP_SELF'] ) );
	
	// stop dit script, want de pagina wordt opnieuw geladen...
	exit();
}













