<?php
/**
 * Functie om strings van formulieren onschadelijk te maken
 * @param  string $data 	de verdachte data
 * @return string      		de sanitized string;
 */
function sanit_input( $data = '' ){
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

/**
 * testValue controleert en saniteerd de waarde van een formulierveld
 * @param  string $naamVeld
 * @return string     		de value of lege string      
 */
function testValue( $naamVeld = ''){
	if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) ){
		return sanit_input( $_POST[$naamVeld] ); // early return!!!!
 	}
 	return '';
}

/**
 * testOption controleert een option van een select formulierveld
 * @param  string $naamVeld
 * @param  string $value   
 * @return string           selected of lege string
 */
function testOption( $naamVeld = '', $value='' ){
	if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) && $value == $_POST[$naamVeld]){
		return 'selected';
 	}
 	return '';
}

/**
 * testRadio controleert een option van een select formulierveld
 * @param  string $naamVeld
 * @param  string $value   
 * @param  boolean $default    
 * @return string           selected of lege string
 */
function testRadio( $naamVeld = '', $value='', $default=false ){
	if (   isset($_POST[$naamVeld])  		// is er een post van de gender 
		&& !empty($_POST[$naamVeld])  		// is hij niet leeg
		&& $value == $_POST[$naamVeld] ){ 	// en matched de waarde van hetveld met de posted value
		return 'checked';
 	} elseif (empty($_POST) && $default){	// nog geen post en WEL default(true)
		return 'checked'; 
 	}  
 	return '';
}

/**
 * testCheck controleert een option van een select formulierveld
 * @param  string $naamVeld -> Een array met waarden
 * @param  string $value   
 * @param  boolean $default    
 * @return string           selected of lege string
 */
function testCheck( $naamVeld = '', $value='', $default=false ){
	if (   isset($_POST[$naamVeld]) 
		&& !empty($_POST[$naamVeld]) 
		&& in_array($value, $_POST[$naamVeld]) ){
		return 'checked';
 	} elseif (empty($_POST) && $default){
		return 'checked'; 
 	}  
 	return '';
}


/**
 * valideer een formulier
 * @return bool // array
 */
function valideerForm(){

	if (isset($_POST['verstuur'])) {

		$valideer = true;
		// $error = [];
		$naamVeld = 'email';
		$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_EMAIL);
		if (isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) && filter_var($_POST[$naamVeld], FILTER_VALIDATE_EMAIL)){
			// het is goed
			// positieve feedback
		} else {
			// het is fout
			// $error[] = 'Verkeerd of geen email ingevuld'
			// negatieve feedback
			$valideer = false;
		}

		$naamVeld = 'voornaam';
		$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_STRING);
		if ( !isset($_POST[$naamVeld]) || empty($_POST[$naamVeld]) ){
			// het is fout
			// $error[] = 'Verkeerde of geen voornaam ingevuld'
			$valideer = false;
		} /* else {
			// dan is het goed en dan evt toch postieve feedback
		}
		*/
	
		$naamVeld = 'achternaam';
		$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_STRING);
		if ( !isset($_POST[$naamVeld]) || empty($_POST[$naamVeld]) )
			$valideer = false;
		
		$naamVeld = 'woonplaats';
		$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_STRING);
		if ( !isset($_POST[$naamVeld]) || empty($_POST[$naamVeld]) )
			$valideer = false;
		




		return $valideer; // goed of fout afhankelijk van de validatie
		// return $error; 
	}
	return false; // geen formulier dus altijd fout!
	// return ['eerstekeer' => true];
}

$formCorrect = valideerForm();