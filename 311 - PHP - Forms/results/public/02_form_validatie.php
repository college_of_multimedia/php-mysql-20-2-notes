<?php require_once '../Library/settings.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<style>
		label {
				display:inline-block; 
				width:100px;
			}
	</style>
</head>
<body>

<!-- 
	<?php print_r($_GET); ?>
-->	
<!--
	<?php print_r($_POST); ?>
-->	
<!-- Beter deze niet gebruiken!!!
	<?php print_r($_REQUEST); ?>
-->

<h1>HTML Formulier Validatie</h1>
<h2>Opdracht 1</h2>
<p>
	Op basis van opgave 01_html_formulier komen nu de volgende validaties:<br>
	- Is het email een geldig email adres?<br>
	- Als het niet een echt email adres is dan moet er een waarschuwing weergegeven worden bij het email veld.<br>
	- maar ... het veld moet wel ingevuld blijven met de waarde die ik verstuurd heb<br>
    filter_var( $email, FILTER_VALIDATE_EMAIL );
</p>

<form action="<?= htmlspecialchars( $_SERVER['PHP_SELF'] ); ?>?pagina=3" method="post" name="myForm" id="myForm">
	<label for="voornaam">Voornaam:</label>
	<input type="text" name="voornaam" id="voornaam" value="<?= testValue('voornaam'); ?>"><br>
	
	<input type="hidden" name="pagina" id="pagina" value="99"><br>

	<label for="tussenvoegsel">Tussenvoegsel:</label>
	<input type="text" name="tussenvoegsel" id="tussenvoegsel" value="<?= testValue('tussenvoegsel'); ?>"><br>

	<label for="achternaam">Achternaam:</label>
	<input type="text" name="achternaam" id="achternaam" value="<?= testValue('achternaam'); ?>"><br>

	<label for="email">Email:</label>
	<input type="text" name="email" id="email" value="<?= testValue('email'); ?>"><br>

	<label for="postcode">Postcode:</label>
	<input type="text" name="postcode" id="postcode" value="<?= testValue('postcode'); ?>"><br>

	<label for="woonplaats">Woonplaats:</label>
	<select name="woonplaats" id="woonplaats">
		<option value="" <?= testOption('woonplaats', '') ?>>Maak een keuze</option>
		<option value="Amsterdam" <?= testOption('woonplaats', 'Amsterdam') ?>>Amsterdam</option>
		<option value="Rotterdam" <?= testOption('woonplaats', 'Rotterdam') ?>>Rotterdam</option>
		<option value="Utrecht" <?= testOption('woonplaats', 'Utrecht') ?>>Utrecht</option>
		<option value="Lelystad" <?= testOption('woonplaats', 'Lelystad') ?>>Lelystad</option>
	</select><br>

	<label for="gender">Gender:</label>
	<input type="radio" name="gender" id="gender_m" value="Man" <?= testRadio('gender', 'Man'); ?>>Man - 
	<input type="radio" name="gender" id="gender_v" value="Vrouw" <?= testRadio('gender', 'Vrouw'); ?>>Vrouw - 
	<input type="radio" name="gender" id="gender_x" value="Neutraal" <?= testRadio('gender', 'Neutraal', true); ?>>Neutraal<br> 

	<label for="computer[]">Computer:</label>
	<input type="checkbox" name="computer[]" id="computer_1" value="Mac" <?= testCheck('computer', 'Mac', true); ?>>Macintosh - 
	<input type="checkbox" name="computer[]" id="computer_2" value="PC" <?= testCheck('computer', 'PC'); ?>>Personal Computer Windows -  
	<input type="checkbox" name="computer[]" id="computer_3" value="Unix" <?= testCheck('computer', 'Unix'); ?>>Unix zelfbouw<br> 

	<label for="verstuur"></label>
	<input type="submit" name="verstuur" value="Verstuur!">

</form>

<h2>Opdracht 2</h2>
<p>
	Als de ingevoerde data valid is, dus door de controles heen komt dan:<br>
	Moet je een bedankt pagina laten zien, anders zie je dus nog steeds het formulier.<br>
	Zorg dat deze pagina een losse php pagina is zodat die goed opgemaakt kan worden.<br>
</p>

<?php

	
	/**
	 * Functie om strings van formulieren onschadelijk te maken
	 * @param  string $data 	de verdachte data
	 * @return string      		de sanitized string;
	 */
	function sanit_input( $data = '' ){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}


	/**
	 * testValue controleert en saniteerd de waarde van een formulierveld
	 * @param  string $naamVeld
	 * @return string     		de value of lege string      
	 */
	function testValue( $naamVeld = ''){
		if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) ){
			return sanit_input( $_POST[$naamVeld] ); // early return!!!!
	 	}
	 	return '';
	}

	/**
	 * testOption controleert een option van een select formulierveld
	 * @param  string $naamVeld
	 * @param  string $value   
	 * @return string           selected of lege string
	 */
	function testOption( $naamVeld = '', $value='' ){
		if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) && $value == $_POST[$naamVeld]){
			return 'selected';
	 	}
	 	return '';
	}

	/**
	 * testRadio controleert een option van een select formulierveld
	 * @param  string $naamVeld
	 * @param  string $value   
	 * @param  boolean $default    
	 * @return string           selected of lege string
	 */
	function testRadio( $naamVeld = '', $value='', $default=false ){
		if (   isset($_POST[$naamVeld])  		// is er een post van de gender 
			&& !empty($_POST[$naamVeld])  		// is hij niet leeg
			&& $value == $_POST[$naamVeld] ){ 	// en matched de waarde van hetveld met de posted value
			return 'checked';
	 	} elseif (empty($_POST) && $default){	// nog geen post en WEL default(true)
			return 'checked'; 
	 	}  
	 	return '';
	}

	/**
	 * testCheck controleert een option van een select formulierveld
	 * @param  string $naamVeld -> Een array met waarden
	 * @param  string $value   
	 * @param  boolean $default    
	 * @return string           selected of lege string
	 */
	function testCheck( $naamVeld = '', $value='', $default=false ){
		if (   isset($_POST[$naamVeld]) 
			&& !empty($_POST[$naamVeld]) 
			&& in_array($value, $_POST[$naamVeld]) ){
			return 'checked';
	 	} elseif (empty($_POST) && $default){
			return 'checked'; 
	 	}  
	 	return '';
	}


	/**
	 * valideer een formulier
	 * @return bool // array
	 */
	function valideerForm(){

		if (isset($_POST['verstuur'])) {

			$valideer = true;
			// $error = [];
			$naamVeld = 'email';
			$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_EMAIL);
			if (isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) && filter_var($_POST[$naamVeld], FILTER_VALIDATE_EMAIL)){
				// het is goed
				// positieve feedback
			} else {
				// het is fout
				// $error[] = 'Verkeerd of geen email ingevuld'
				// negatieve feedback
				$valideer = false;
			}

			$naamVeld = 'voornaam';
			$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_STRING);
			if ( !isset($_POST[$naamVeld]) || empty($_POST[$naamVeld]) ){
				// het is fout
				// $error[] = 'Verkeerde of geen voornaam ingevuld'
				$valideer = false;
			} /* else {
				// dan is het goed en dan evt toch postieve feedback
			}
			*/
		
			$naamVeld = 'achternaam';
			$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_STRING);
			if ( !isset($_POST[$naamVeld]) || empty($_POST[$naamVeld]) )
				$valideer = false;
			
			$naamVeld = 'woonplaats';
			$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_STRING);
			if ( !isset($_POST[$naamVeld]) || empty($_POST[$naamVeld]) )
				$valideer = false;
			




			return $valideer; // goed of fout afhankelijk van de validatie
			// return $error; 
		}
		return false; // geen formulier dus altijd fout!
		// return ['eerstekeer' => true];
	}

$formCorrect = valideerForm();

if ($formCorrect){
	// jump-in-out
?>
<hr>
<h2>Dit is onze nieuwe pagina</h2>
<?php 
	if ( !empty( $_POST ) ){
		foreach ( $_POST as $key => $value ) {
			if ( is_string( $value ) ) {
				$_POST[ $key ] = sanit_input( $value ); 
			}
		}
		// alles is "veilig"
		$aanhef = '';
		if (isset($_POST['gender'])){
			if ($_POST['gender'] == 'Man'){
				$aanhef = 'meneer';
			} elseif ($_POST['gender'] == 'Vrouw'){
				$aanhef = 'mevrouw';
			}
		}

		$vollenaam = trim($_POST['voornaam'] . ' ' . $_POST['tussenvoegsel']) . ' ' . $_POST['achternaam'] ;

		$computers = 'geen computer!';
		if (isset($_POST['computer'])){
			$computers = implode($_POST['computer'], ', ');
		}


		$bericht =  "<p>Hallo $aanhef $vollenaam uit {$_POST['woonplaats']}, je gebruikt $computers</p>";
		echo $bericht;

	/*		$result = mail('harald@cmm.nietdoen', 'onderwerp', $bericht);

		if ($result) {
			// een redirect:
			header('Location: ' . '/danku.php');
		}
	*/
	}
} else {
	// deel 2 van de jump-in-out
?>	
<hr>
<h2>U dient het formulier in te vullen</h2>
<?php	 
}
?>
</body>
</html>
