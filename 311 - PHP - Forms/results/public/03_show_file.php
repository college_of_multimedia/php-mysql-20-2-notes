<h1>Bestanden versturen</h1>
<h2>Opdracht 1</h2>
<p>
    Maak een nieuwe php pagina met daarin een html formulier<br>
    In dit formulier moet het mogelijk zijn om een bestand te selecteren en te versturen<br>
</p>
<style>
	label {
		display: inline-block;
		width: 100px;
	}
</style>

<form 
	action="<?= htmlspecialchars($_SERVER['PHP_SELF']); ?>"
	method="post"
	enctype="multipart/form-data"
	id="formNaam"
	name="formNaam" >
	<label for="bestand">File:</label>
	<input type="file" name="bestand" id="bestand"><br>
	<label for="verstuur"></label>
	<input type="submit" name="verstuur" id="verstuur" value="Verstuur"><br>
</form>

<!-- <?php print_r($_FILES); ?> -->

<h2>Opdracht 2</h2>
<p>
    Sla het verstuurde bestand op als het verstuurd is<br>
    v.b.: <br>
    move_uploaded_file( $_FILES['bestandje']['tmp_name'], $_FILES['bestandje']['name'] );
</p>

<?php
	// $uploaddir = __DIR__ . '/upload/';
	// $filename = $_FILES['bestand']['name'];

	// move_uploaded_file( $_FILES['bestand']['tmp_name'], $uploaddir . $filename );
?>


<h2>Opdracht 3</h2>
<p>
    Zorg ervoor dat er alleen png en jpg bestanden verstuurd kunnen worden.<br>
    array('image/jpeg', 'image/gif', 'image/png');
</p>

<?php
/*
	$uploaddir = __DIR__ . '/upload/';
	$max_filesize = 15240; // ongeveer 15KB maar in bytes
	$naam_klant = "harald"; // in echt uit de database
	$validMimeTypes = array('image/jpeg', 'image/gif', 'image/png');
	// $validMimeTypes = array('image/jpeg' => '.jpg', 'image/gif' => '.gif', 'image/png' => '.png');

	if (isset($_FILES['bestand']) && 0 === $_FILES['bestand']['error']){ 
		$file = $_FILES['bestand'];
		$filename = $file['name'];

		if ( in_array($file['type'], $validMimeTypes ) 
			&& $file['size'] <= $max_filesize ){
			$nieuwenaam = $uploaddir . $naam_klant . '_' . $filename;
			move_uploaded_file( $file['tmp_name'], $nieuwenaam );
		} else {
			echo 'Ongeldig, check bestandstype of de maximale grootte (15KB)';
		}

	}
	*/
?>



<h2>Opdracht 4</h2>
<p>
    Zorg ervoor dat de bestanden nooit overschreven worden.<br>
</p>

<?php
	$uploaddir = __DIR__ . '/upload/';
	$max_filesize = 15240; // ongeveer 15KB maar in bytes
	$naam_klant = "harald"; // in echt uit de database
	$validMimeTypes = array('image/jpeg', 'image/gif', 'image/png');
	// $validMimeTypes = array('image/jpeg' => '.jpg', 'image/gif' => '.gif', 'image/png' => '.png');

	if (isset($_FILES['bestand']) && 0 === $_FILES['bestand']['error']){ 
		$file = $_FILES['bestand'];
		$filename = $file['name'];

		if ( in_array($file['type'], $validMimeTypes ) 
			&& $file['size'] <= $max_filesize ){
			$nieuwenaam = $uploaddir . $naam_klant . '_' . $filename;
			if (file_exists($nieuwenaam)){
				echo '<p>Het bestand bestaat al!</p>';
			} else {
				move_uploaded_file( $file['tmp_name'], $nieuwenaam );
			}
		} else {
			echo 'Ongeldig, check bestandstype of de maximale grootte (15KB)';
		}

	}
	
?>
<h2>Opdracht 5 voor de liefhebbers</h2>
<p>
    Zorg ervoor dat de bestanden nooit overschreven worden. MAAAR er iets achter zet<br>
</p>








