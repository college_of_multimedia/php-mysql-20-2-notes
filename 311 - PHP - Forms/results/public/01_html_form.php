<?php require_once '../Library/settings.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<style>
		label {
				display:inline-block; 
				width:100px;
			}
	</style>
</head>
<body>

<pre><?php print_r($_GET); ?></pre>	
<pre><?php print_r($_POST); ?></pre>	

<h1>HTML Formulier</h1>
<h2>Opdracht 1</h2>
<p>
	Maak een HTML formulier waarbij ik een voornaam, tussenvoegsel, achternaam, email en postcode kan invoeren<br>
	Ook wil ik een woonplaats kunnen selecteren <em>( 3 opties zijn genoeg )</em><br>
	Als ik op 'verstuur' klik dan moet ik op dezelfde pagina komen<br>
</p>

<form action="<?= htmlspecialchars( $_SERVER['PHP_SELF'] ); ?>" method="post" name="myForm" id="myForm">
	<label for="voornaam">Voornaam:</label>
	<input type="text" name="voornaam" id="voornaam" value="<?= testValue('voornaam'); ?>"><br>

	<label for="tussenvoegsel">Tussenvoegsel:</label>
	<input type="text" name="tussenvoegsel" id="tussenvoegsel" value="<?= testValue('tussenvoegsel'); ?>"><br>

	<label for="achternaam">Achternaam:</label>
	<input type="text" name="achternaam" id="achternaam" value="<?= testValue('achternaam'); ?>"><br>

	<label for="email">Email:</label>
	<input type="text" name="email" id="email" value="<?= testValue('email'); ?>"><br>

	<label for="postcode">Postcode:</label>
	<input type="text" name="postcode" id="postcode" value="<?= testValue('postcode'); ?>"><br>

	<label for="woonplaats">Woonplaats:</label>
	<select name="woonplaats" id="woonplaats">
		<option value="Amsterdam" <?= testOption('woonplaats', 'Amsterdam') ?>>Amsterdam</option>
		<option value="Rotterdam" <?= testOption('woonplaats', 'Rotterdam') ?>>Rotterdam</option>
		<option value="Utrecht" <?= testOption('woonplaats', 'Utrecht') ?>>Utrecht</option>
		<option value="Lelystad" <?= testOption('woonplaats', 'Lelystad') ?>>Lelystad</option>
	</select><br>

	<label for="gender">Gender:</label>
	<input type="radio" name="gender" id="gender_m" value="Man" <?= testRadio('gender', 'Man'); ?>>Man - 
	<input type="radio" name="gender" id="gender_v" value="Vrouw" <?= testRadio('gender', 'Vrouw'); ?>>Vrouw - 
	<input type="radio" name="gender" id="gender_x" value="Neutraal" <?= testRadio('gender', 'Neutraal', true); ?>>Neutraal<br> 

	<label for="computer[]">Computer:</label>
	<input type="checkbox" name="computer[]" id="computer_1" value="Mac" <?= testCheck('computer', 'Mac', true); ?>>Macintosh - 
	<input type="checkbox" name="computer[]" id="computer_2" value="PC" <?= testCheck('computer', 'PC'); ?>>Personal Computer Windows -  
	<input type="checkbox" name="computer[]" id="computer_3" value="Unix" <?= testCheck('computer', 'Unix'); ?>>Unix zelfbouw<br> 

	<label for="verstuur"></label>
	<input type="submit" name="verstuur" value="Verstuur!">

</form>

<h2>Opdracht 2</h2>
<p>
	Na het versturen moeten de ingevulde waardes weer zichtbaar zijn in het formulier.<br>
    Ook de juiste selectie optie moet geselecteerd worden
</p>
<?php

	/**
	 * testValue controleert en saniteerd de waarde van een formulierveld
	 * @param  string $naamVeld
	 * @return string     		de value of lege string      
	 */
	function testValue( $naamVeld = ''){
		if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) ){
			return trim( $_POST[$naamVeld] ); // early return!!!!
	 	}
	 	return '';
	}

	/**
	 * testOption controleert een option van een select formulierveld
	 * @param  string $naamVeld
	 * @param  string $value   
	 * @return string           selected of lege string
	 */
	function testOption( $naamVeld = '', $value='' ){
		if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) && $value == $_POST[$naamVeld]){
			return 'selected';
	 	}
	 	return '';
	}

	/**
	 * testRadio controleert een option van een select formulierveld
	 * @param  string $naamVeld
	 * @param  string $value   
	 * @param  boolean $default    
	 * @return string           selected of lege string
	 */
	function testRadio( $naamVeld = '', $value='', $default=false ){
		if (   isset($_POST[$naamVeld])  		// is er een post van de gender 
			&& !empty($_POST[$naamVeld])  		// is hij niet leeg
			&& $value == $_POST[$naamVeld] ){ 	// en matched de waarde van hetveld met de posted value
			return 'checked';
	 	} elseif (empty($_POST) && $default){	// nog geen post en WEL default(true)
			return 'checked'; 
	 	}  
	 	return '';
	}

	/**
	 * testCheck controleert een option van een select formulierveld
	 * @param  string $naamVeld -> Een array met waarden
	 * @param  string $value   
	 * @param  boolean $default    
	 * @return string           selected of lege string
	 */
	function testCheck( $naamVeld = '', $value='', $default=false ){
		if (   isset($_POST[$naamVeld]) 
			&& !empty($_POST[$naamVeld]) 
			&& in_array($value, $_POST[$naamVeld]) ){
			return 'checked';
	 	} elseif (empty($_POST) && $default){
			return 'checked'; 
	 	}  
	 	return '';
	}


?>
<h2>Opdracht 3</h2>
<p>
	Laat het formulier nu naar een nieuwe pagina verwijzen<br>
    Geef op deze nieuwe pagina de ingevulde waardes weer.
</p>
<hr>
<h2>Dit is onze nieuwe pagina</h2>
<?php 
if ( !empty( $_POST ) ){
	foreach ( $_POST as $key => $value ) {
		if ( is_string( $value ) ) {
			$_POST[ $key ] = trim( $value ); // of betere sanitize!!!
		}
	}
	// alles is "veilig"
	$aanhef = '';
	if (isset($_POST['gender'])){
		if ($_POST['gender'] == 'Man'){
			$aanhef = 'meneer';
		} elseif ($_POST['gender'] == 'Vrouw'){
			$aanhef = 'mevrouw';
		}
	}

	$vollenaam = trim($_POST['voornaam'] . ' ' . $_POST['tussenvoegsel']) . ' ' . $_POST['achternaam'] ;

	$computers = 'geen computer!';
	if (isset($_POST['computer'])){
		$computers = implode($_POST['computer'], ', ');
	}


	$bericht =  "<p>Hallo $aanhef $vollenaam uit {$_POST['woonplaats']}, je gebruikt $computers</p>";
	echo $bericht;

/*		$result = mail('harald@cmm.nietdoen', 'onderwerp', $bericht);

	if ($result) {
		// een redirect:
		header('Location: ' . '/danku.php');
	}
*/
}

?>

</body>
</html>
