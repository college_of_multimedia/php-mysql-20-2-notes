<?php

$nl = "\n";
$br = '<br>' . $nl; 
$hr = '<hr>' . $nl;

/**
 * debuggen laat een nette dump van een variabel zien
 * eventueel met bestandsnaam en regelnummer
 * @param  mixed  $val  de te tonen variabel
 * @param  string $file bestandsnaam van het script
 * @param  string $line regelnummer van de dump
 * @return void         geen return dus
 */
function debuggen($val, $file='', $line=''){
	global $debug,$nl;
	if ($debug){
		echo '<!--' . $nl;
		if ( '' !== $file ){
			echo "File: $file on line: $line \n";
		}
		var_dump($val);
		echo '-->' . $nl;
	}
}
// geen echo of uitvoer in de root van dit script en ook niet afsluiten