<?php
$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$nl = "\n";
$br = "<br>\n";
$tb = "\t";

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'cmm_wd317_autos');

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	if ( $_mysqli->connect_errno ){
		die('Failed to connect to database: ' . $_mysqli->connect_errno );
	}
	return $_mysqli;
}

$mysqliObject = connectToDB();

$order  = 'title';
$direction = "ASC";

if ( isset($_GET['sort']) ){
	$order  = $_GET['sort'];
}

$query = 'SELECT a.title, m.naam AS merk, m.land, a.type, a.kleur, a.brandstof, a.zitplaatsen, a.prijs 
		  FROM  `autos` AS a
		  LEFT JOIN `merken` AS m
		  ON m.id = a.merk_id
		  ORDER BY ' . $order . ' ' . $direction . ' , type ' ;
echo '<h1> OPDRACHT 6</h1>';

echo 'Query: ', $query, $br, $br;

$mysqliResult = $mysqliObject->query($query);

echo 'Aantal items = ', $mysqliResult->num_rows, $br, $br;

$auto = $mysqliResult->fetch_assoc();

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th><a href="?sort=title"		>title</a></th>
		<th><a href="?sort=merk"		>merk</a></th>
		<th><a href="?sort=land"		>land</a></th>
		<th><a href="?sort=type"		>type</a></th>
		<th><a href="?sort=kleur"		>kleur</a></th>
		<th><a href="?sort=brandstof"	>brandstof</a></th>
		<th><a href="?sort=zitplaatsen"	>zitplaatsen</a></th>
		<th><a href="?sort=prijs"		>prijs</a></th>
	</tr>
';

do {
	echo '		<tr>', $nl;
	echo '			<td>' . $auto['title'] . '</td>', $nl;
	echo '			<td>' . $auto['merk'] . '</td>', $nl;
	echo '			<td>' . $auto['land'] . '</td>', $nl;
	echo '			<td>' . $auto['type'] . '</td>', $nl;
	echo '			<td>' . $auto['kleur'] . '</td>', $nl;
	echo '			<td>' . $auto['brandstof'] . '</td>', $nl;
	echo '			<td>' . $auto['zitplaatsen'] . '</td>', $nl;
	echo '			<td>' . $auto['prijs'] . '</td>', $nl;
	echo '		</tr>', $nl;
} while( $auto = $mysqliResult->fetch_assoc() );

echo	'</table>';
