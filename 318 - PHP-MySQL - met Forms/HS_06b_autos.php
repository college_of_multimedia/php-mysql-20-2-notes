<?php
$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$nl = "\n";
$br = "<br>\n";
$tb = "\t";

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'cmm_wd317_autos');

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	if ( $_mysqli->connect_errno ){
		die('Failed to connect to database: ' . $_mysqli->connect_errno );
	}
	return $_mysqli;
}

$mysqliObject = connectToDB();

$order  = 'title'; // default order 
 
$direction = "ASC"; // default direction 

if ( isset($_GET['sort']) ){
	$order  = $_GET['sort'];
}

$query = 'SELECT a.title, m.naam AS merk, m.land, a.type, a.kleur, a.brandstof, a.zitplaatsen, a.prijs 
		  FROM  `autos` AS a
		  LEFT JOIN `merken` AS m
		  ON m.id = a.merk_id
		  ORDER BY ' . $order . ' ' . $direction . ' , type ' ;

// $query = 'SELECT * FROM  `merken` ORDER BY `naam`';

echo '<h1> OPDRACHT 6b</h1>';

// echo 'Query: ', $query, $br, $br;

$mysqliResult = $mysqliObject->query($query);

// echo 'Aantal items = ', $mysqliResult->num_rows, $br, $br;

$object = $mysqliResult->fetch_assoc();

$tabelHeader = '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
';

foreach ($object as $key => $value) {
	$tabelHeader .= $tb . $tb. '<th><a href="?sort=' . $key . '">' . $key . '</a></th>' . $nl;
}

$tabelHeader .=	$tb . '</tr>' .  $nl;

echo $tabelHeader;

do {
	echo '	<tr>', $nl;
	foreach ($object as $key => $value) {
		echo '		<td>' . $value . '</td>', $nl;
	}
	echo '	</tr>', $nl;
} while( $object = $mysqliResult->fetch_assoc() );

echo	'</table>';
