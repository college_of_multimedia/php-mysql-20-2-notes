<h1>Gecombineerde tabellen weergeven</h1>
<p>
    Op basis van php_mysql_voorbeeld.php <br>
    wil ik uiteindelijk een combinatie zien van autos en merken<br>
</p>

<h2>Opdracht 1</h2>
<p>
	Importeer de database uit php_mysql_voorbeeld.php<br>
</p>

<h2>Opdracht 2</h2>
<p>
	Maak verbinding met deze database en laat de merken zien.<br>
</p>

<?php
$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$nl = "\n";
$br = "<br>\n";
$tb = "\t";

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'cmm_wd317_autos');

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	if ( $_mysqli->connect_errno ){
		die('Failed to connect to database: ' . $_mysqli->connect_errno );
	}
	return $_mysqli;
}
## opdracht 1

## opdracht 2

$mysqliObject = connectToDB();

$query = 'SELECT * FROM  `merken` ORDER BY `naam`';
echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query);

// die(var_dump($mysqliResult));
echo 'Aantal items = ', $mysqliResult->num_rows, $br;

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>id</th>
		<th>merk</th>
		<th>land</th>
	</tr>', $nl;
while( $row = $mysqliResult->fetch_assoc() ){
	echo '		<tr>', $nl;
	echo '			<td>' . $row['id'] . '</td>', $nl;
	echo '			<td>' . $row['naam'] . '</td>', $nl;
	echo '			<td>' . $row['land'] . '</td>', $nl;
	echo '		</tr>', $nl;
}
echo '</table>',$nl;
?>
<h2>Opdracht 3</h2>
<p>
	Laat nu alle auto's zien in een overzicht<br>
</p>

<?php
## opdracht 3

$query = 'SELECT * FROM  `autos` ORDER BY `id`';
echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query);

// die(var_dump($mysqliResult));
echo 'Aantal items = ', $mysqliResult->num_rows, $br;

// `id`, `merk_id`, `title`, `type`, `kleur`, `brandstof`, `zitplaatsen`, `prijs`

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>id</th>
		<th>merk_id</th>
		<th>title</th>
		<th>type</th>
		<th>kleur</th>
		<th>brandstof</th>
		<th>zitplaatsen</th>
		<th>prijs</th>
	</tr>', $nl;
while( $auto = $mysqliResult->fetch_assoc() ){
	echo '		<tr>', $nl;
	echo '			<td>' . $auto['id'] . '</td>', $nl;
	echo '			<td>' . $auto['merk_id'] . '</td>', $nl;
	echo '			<td>' . $auto['title'] . '</td>', $nl;
	echo '			<td>' . $auto['type'] . '</td>', $nl;
	echo '			<td>' . $auto['kleur'] . '</td>', $nl;
	echo '			<td>' . $auto['brandstof'] . '</td>', $nl;
	echo '			<td>' . $auto['zitplaatsen'] . '</td>', $nl;
	echo '			<td>' . $auto['prijs'] . '</td>', $nl;
	echo '		</tr>', $nl;
}
echo '</table>',$nl;

?>
<h2>Opdracht 4</h2>
<p>
	Maak een 3e tabel op de html pagina.<br>
    Hier moet een combinatie zichtbaar zijn van de auto's en merken, bv:<br>
<?php

$query = 'SELECT a.title, m.naam AS merk, m.land, a.type, a.kleur, a.brandstof, a.zitplaatsen, a.prijs 
		  FROM  `autos` AS a
		  LEFT JOIN `merken` AS m
		  ON m.id = a.merk_id';

echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query);

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>title</th>
		<th>merk</th>
		<th>land</th>
		<th>type</th>
		<th>kleur</th>
		<th>brandstof</th>
		<th>zitplaatsen</th>
		<th>prijs</th>
	</tr>
';

// var_dump($mysqliResult->fetch_assoc()); // haal een row op
// $mysqliResult = $mysqliObject->query($query);
// var_dump($mysqliResult->fetch_assoc()); // haal de volgende row op!!
$autoArray = [];

while( $auto = $mysqliResult->fetch_assoc() ){
	echo '		<tr>', $nl;
	echo '			<td>' . $auto['title'] . '</td>', $nl;
	echo '			<td>' . $auto['merk'] . '</td>', $nl;
	echo '			<td>' . $auto['land'] . '</td>', $nl;
	echo '			<td>' . $auto['type'] . '</td>', $nl;
	echo '			<td>' . $auto['kleur'] . '</td>', $nl;
	echo '			<td>' . $auto['brandstof'] . '</td>', $nl;
	echo '			<td>' . $auto['zitplaatsen'] . '</td>', $nl;
	echo '			<td>' . $auto['prijs'] . '</td>', $nl;
	echo '		</tr>', $nl;
	$autoArray[] = $auto;
}

echo	'</table>';

?>


</p>

<h2>Opdracht 5</h2>
<p>
	Kun je ervoor zorgen dat de sortering van de tabel aangepast door de een get variabele mee te sturen<br>
    Je url wordt dan bijvoorbeeld: <a href="?sort=title">http://cmm-lessen.dev/01_autos.php?sort=title</a><br>
    of <a href="?sort=prijs">http://cmm-lessen.dev/01_autos.php?sort=prijs</a>
</p>

<?php
$order  = 'title';

if ( isset($_GET['sort']) ){
	$order  = $_GET['sort'];
}

$query = 'SELECT a.title, m.naam AS merk, m.land, a.type, a.kleur, a.brandstof, a.zitplaatsen, a.prijs 
		  FROM  `autos` AS a
		  LEFT JOIN `merken` AS m
		  ON m.id = a.merk_id
		  ORDER BY ' . $order;



echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query);

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>title</th>
		<th>merk</th>
		<th>land</th>
		<th>type</th>
		<th>kleur</th>
		<th>brandstof</th>
		<th>zitplaatsen</th>
		<th>prijs</th>
	</tr>
';

// var_dump($mysqliResult->fetch_assoc()); // haal een row op
// $mysqliResult = $mysqliObject->query($query);
// var_dump($mysqliResult->fetch_assoc()); // haal de volgende row op!!
$autoArray = [];

while( $auto = $mysqliResult->fetch_assoc() ){
	echo '		<tr>', $nl;
	echo '			<td>' . $auto['title'] . '</td>', $nl;
	echo '			<td>' . $auto['merk'] . '</td>', $nl;
	echo '			<td>' . $auto['land'] . '</td>', $nl;
	echo '			<td>' . $auto['type'] . '</td>', $nl;
	echo '			<td>' . $auto['kleur'] . '</td>', $nl;
	echo '			<td>' . $auto['brandstof'] . '</td>', $nl;
	echo '			<td>' . $auto['zitplaatsen'] . '</td>', $nl;
	echo '			<td>' . $auto['prijs'] . '</td>', $nl;
	echo '		</tr>', $nl;
	$autoArray[] = $auto;
}

echo	'</table>';

?>


<h2>Opdracht 6</h2>
<p>
	Kun je ervoor zorgen dat als je op de titel van een kolom klikt je sorteert op die kolom?<br>
</p>


