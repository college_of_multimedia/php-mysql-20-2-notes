<?php
$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$nl = "\n";
$br = "<br>\n";
$tb = "\t";

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'cmm_wd317_autos');

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	if ( $_mysqli->connect_errno ){
		die('Failed to connect to database: ' . $_mysqli->connect_errno );
	}
	return $_mysqli;
}

$mysqliObject = connectToDB();

// http://192.168.33.34/HS_06c_autos.php?sort=prijs&zoek=b&direction=ASC


$order  = 'title'; // default order 
 
$direction = "ASC"; // default direction 

$zoek = '%%';

if ( isset($_GET['zoek']) ){
	$zoek  = "%" . $_GET['zoek'] . "%";
}

if ( isset($_GET['sort']) ){
	$order  = $_GET['sort'];
}

	$query = 'SELECT a.title, m.naam AS merk, m.land, a.type, a.kleur, a.brandstof, a.zitplaatsen, a.prijs 
		  FROM  `autos` AS a
		  LEFT JOIN `merken` AS m
		  ON m.id = a.merk_id
		  WHERE m.naam LIKE ? OR a.title LIKE ? OR a.type LIKE ?
		  ORDER BY ' . $order . ' ' . $direction;


	###### 3- bereid een query voor om uit te voeren
	
	if ( ! $stmt = $mysqliObject->prepare($query) ) {
		throw new Exception('Prepare Failed: ' . $mysqliObject->errno);
	}

	###### 4- bind de parameters en geef aan welk type het item is
	
	if ( ! $stmt->bind_param('sss', $zoek, $zoek, $zoek) ) {
		throw new Exception('Bind Failed: ' . $mysqliObject->errno);
	}

	###### 5- voer de query uit
	
	if ( ! $stmt->execute() ) {
		throw new Exception('Execute Failed: ' . $mysqliObject->errno);
	}

	###### 6- haal de gegevens op
	
	$result = $stmt->get_result();


echo '<h1> OPDRACHT 6C</h1>';

// echo 'Aantal items = ', $mysqliResult->num_rows, $br, $br;

$object = $result->fetch_assoc();

$tabelHeader = '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
';

foreach ($object as $key => $value) {
	$tabelHeader .= $tb . $tb. '<th><a href="?sort=' . $key . '">' . $key . '</a></th>' . $nl;
}

$tabelHeader .=	$tb . '</tr>' .  $nl;

echo $tabelHeader;

do {
	echo '	<tr>', $nl;
	foreach ($object as $key => $value) {
		echo '		<td>' . $value . '</td>', $nl;
	}
	echo '	</tr>', $nl;
} while( $object = $result->fetch_assoc() );

echo	'</table>';
/*
De prepare blijft bestaan, en kan nog een keer worden gebind
if ( ! $stmt->bind_param('sss', $zoek2, $zoek2, $zoek2) ) {
	throw new Exception('Bind Failed: ' . $mysqliObject->errno);
}
 */



