<h1>Arrays - Statements.</h1>
<h2>Opdracht 1</h2>
Laat alle items uit de onderstaande array zien<br>

<h2>Opdracht 2</h2>
Laat alleen Karel zien<br>

<h2>Opdracht 3</h2>
Laat iedereen zien behalve Kees<br>

<h2>Opdracht 4</h2>
hoeveel regels zitten er in de array?<br>

<h2>Opdracht 5</h2>
Geef alle namen netjes onder elkaar weer.<br>
Als iemand geen tussenvoegsel heeft dan mag er maat 1 spatie zitten tussen de voor en achternaam<br>

<br>
<br>

<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL); // ik wil ALLE errors zien..
// error_reporting(E_ALL & ~E_NOTICE); // ik wil alle errors zien, behalve de notices..

$nl = "\n";
$br = '<br>' . $nl;
$hr = '<hr>' . $nl;

// de array
$myArr = array();
$myArr[] = array( 'voornaam'=>'kees', 	'tussen'=>'de',	'achternaam'=>'vries');
$myArr[] = array( 'voornaam'=>'Jan', 	'tussen'=>'', 	'achternaam'=>'bakker');
$myArr[] = array( 'voornaam'=>'Karel', 	'tussen'=>'', 	'achternaam'=>'groenteman');
$myArr[] = array( 'voornaam'=>'Jan', 	'tussen'=>'de', 'achternaam'=>'vries');
$myArr[] = array( 'voornaam'=>'Joop', 	'tussen'=>'de', 'achternaam'=>'bakker');
$myArr[] = array( 'voornaam'=>'piet', 	'tussen'=>'', 	'achternaam'=>'bakker');


echo $hr;
echo '<h2>Oplossing vraag 1:</h2><pre>', $nl;
foreach($myArr as $persoon){
	echo 'Voornaam      : ' . $persoon['voornaam'] . $nl;
	echo "Tussen        : {$persoon['tussen']}$nl";
	echo "Achternaam    : {$persoon['achternaam']}$nl";
	echo $hr;
}

echo $hr;
echo '<h2>Oplossing vraag 2:</h2><pre>', $nl;
foreach($myArr as $persoon){
	if ($persoon['voornaam'] !== 'Kees') continue;
	echo 'Voornaam      : ' . $persoon['voornaam'] . $nl;
	echo "Tussen        : {$persoon['tussen']}$nl";
	echo "Achternaam    : {$persoon['achternaam']}$nl";
	echo $hr;
}

echo $hr;
echo '<h2>Oplossing vraag 3:</h2><pre>', $nl;
foreach($myArr as $persoon){
	if ($persoon['voornaam'] == 'Kees') continue;
	echo 'Voornaam      : ' . $persoon['voornaam'] . $nl;
	echo "Tussen        : {$persoon['tussen']}$nl";
	echo "Achternaam    : {$persoon['achternaam']}$nl";
	echo $hr;
}

echo $hr;
echo '<h2>Oplossing vraag 4:</h2><pre>', $nl;
echo 'Er zijn ' . count($myArr) . ' personen', $nl;

echo 'Er zijn ' . $myArr->length . ' personen', $nl;


