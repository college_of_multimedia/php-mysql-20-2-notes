<?php require_once '../../Library/settings.php'; ?>
<?php
$page_title = 'Greenleaf Visit our Location';
$page_name = 'visit';
?>
<?php require_once INC . '/header.php' ?>

<!-- Begin content -->
				<div id="pullquote-2">
					<h2>By train or plane, you can reach us!</h2>
					
				</div>
			</header>

			<section id="content">
				
				<blockquote class="cta">
					<p>“Greenleaf takes you there”</p>
				</blockquote>
			</section>
<!-- einde content -->

<?php require_once INC . '/footer.php' ?>
