<?php require_once '../Library/settings.php'; ?>
<?php
$page_title = 'Greenleaf Homepage';
$page_name = 'home';
?>
<?php require_once INC . '/header.php' ?>

<!-- Begin content -->
				<div id="pullquote">
					<h2>Are you prepared to be spellbound?</h2>
					<p>"Trevi" meaning three roads in Latin, offers discerning consumers three exciting ways to savor and enjoy the true spirit of Italy.</p>
				</div>
				<div id="slideshow"> 
					<p>A lovely chinese garden with a palace.</p>
				</div>
			</header>

			<section id="content">
				<article>
					<img src="<?= IMG_URL ?>/leaf1.jpg" alt="">
					<img src="<?= IMG_URL ?>/dwell.jpg" alt="Dwell">
					<h2>Dwell</h2>
					<p>Italian adventures for a select group of world travelers seeking authentic experiences layered with tailored activities.</p>
				</article>
				<article> 
					<img src="<?= IMG_URL ?>/leaf2.jpg" alt="">
					<img src="<?= IMG_URL ?>/decorate.jpg" alt="Decorate">
					<h2>Decorate</h2>
					<p>Exclusive Italian-designed, handmade home and giftware selections for style-conscious consumers.</p>
				</article>
				<article> 
					<img src="<?= IMG_URL ?>/leaf3.jpg" alt="">
					<img src="<?= IMG_URL ?>/discover.jpg" alt="Discover">
					<h2>Discover</h2>
					<p>A luxury vacation home in the undiscovered region of Puglia becomes a reality with the support of our multi-lingual, local team.</p>
				</article>
				<blockquote class="cta">
					<p>“Greenleaf takes you there”</p>
				</blockquote>
			</section>
<!-- einde content -->

<?php require_once INC . '/footer.php' ?>
