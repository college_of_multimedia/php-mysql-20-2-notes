<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.5, user-scalable=yes">
		<title><?= $page_title ?></title>
		<link href="<?= CSS_URL ?>/garden-leaf.css" rel="stylesheet">
	</head>
	<body>
		<div id="container">
			<h1 id="logo"><img src="<?= IMG_URL ?>/logo-greenleaf.jpg" alt=""> Greenleaf - Garden Center</h1>
			<nav>
				<label for="showMenu" id="showMenuLabel">Show menu</label>
				<input type="checkbox" id="showMenu" role="button">
				<ul>
<?php include INC . '/navigatie.php' ?>
				</ul>
			</nav>
			<header>
<!-- einde header.php -->
