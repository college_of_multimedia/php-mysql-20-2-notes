<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);

define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_HOST', 'localhost');
define('DB_NAME', 'speelhoek_db');

define('DOCUMENT_ROOT', realpath( $_SERVER['DOCUMENT_ROOT'] ) );
define('IMG_ROOT',      realpath( DOCUMENT_ROOT . '/images' ) );
define('SITE_ROOT',     realpath( DOCUMENT_ROOT . '/..'     ) );
define('LIB_ROOT',      realpath( SITE_ROOT . '/Library'    ) );
define('INC',           realpath( LIB_ROOT . '/Includes'    ) );

// define('WEB_URL', '//' . $_SERVER['HTTP_HOST']);
// leep trucje auto http: of https:
define('WEB_URL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);
define('IMG_URL', WEB_URL . '/images');
define('CSS_URL', WEB_URL . '/css');
define('JS_URL', WEB_URL . '/js');
define('PAGES_URL', WEB_URL . '/pages');

include_once 'functions.php';
// DDDDDUUUUUUS geen close van de PHP en geen uitvoer