CREATE DATABASE IF NOT EXISTS `CMM_People`;

USE `CMM_People`;

CREATE TABLE `People` ( 
	userID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, 
	Name VARCHAR(50) NOT NULL, 
	Email VARCHAR(50), 
	Pass CHAR(32) NOT NULL, 
	RegDate DATETIME NOT NULL, 
	RegDateTM TIMESTAMP NOT NULL, 
	PRIMARY KEY (userID)
);

SHOW TABLES;

SHOW COLUMNS FROM `People`;

INSERT INTO `People` 
VALUES ('', "me", "me@me.me", md5('welkom'), NOW() , now() );


INSERT INTO `People` (Name, Email,Pass, RegDate) VALUES 
("ik", "ik@ik.nl", md5('mijnpass'), NOW() ), 
("jij", "jij@jij.nl", md5("jij"), NOW() ), 
("jij1", "1jij@jij.nl", md5("jij"), NOW() ), 
("jij2", "2jij@jij.nl", md5("jij"), NOW() ), 
("jij3", "3jij@jij.nl", md5("jij"), NOW() ), 
("jij4", "4jij@jij.nl", md5("jij"), NOW() ), 
("jij5", "5jij@jij.nl", md5("jij"), NOW() );

SELECT * FROM `People`;

SELECT Name FROM `People`; 

SELECT Name, Email FROM `People`; 

SELECT * FROM `People` WHERE name = "ik"; 

SELECT * FROM `People` WHERE Email LIKE "%.you"; 

SELECT * FROM `People` WHERE Email NOT LIKE "%.m_";

SELECT * FROM `People` LIMIT 5;

SELECT * FROM `People` LIMIT 5,5; 

SELECT * FROM `People` ORDER BY Name;

SELECT * FROM `People` ORDER BY Name DESC;

UPDATE `People` SET `Email`="new@me.me" WHERE `name` = "me";

ALTER TABLE `People` CHANGE Email email VARCHAR(40) NOT NULL;

ALTER TABLE `People` CHANGE Email Email VARCHAR(40) NOT NULL AFTER Pass;

DELETE FROM `People` WHERE `UserID` = 3; 

DROP TABLE `People` 

DROP DATABASE `CMM_People`

