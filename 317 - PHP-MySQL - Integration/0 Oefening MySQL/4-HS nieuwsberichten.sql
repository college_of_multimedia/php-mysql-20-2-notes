## Database
## Op basis van de nieuwsberichten sql gaan we opdrachten uitvoeren

## Opdracht 1
## Laat alle nieuws berichten zien

USE cmm_nieuwsberichten;
SELECT * FROM `nieuwsberichten`;

SELECT titel, user_id, publicatie_datum FROM `nieuwsberichten`;

## Opdracht 2
## Geef de naam van de schrijver bij elk bericht weer

SELECT n.titel, s.naam, n.publicatie_datum
FROM `nieuwsberichten` AS n
INNER JOIN schrijvers AS s
ON n.user_id = s.id;

## Opdracht 3
## Haal alleen de berichten op van de gebruiker jij

SELECT n.titel, s.naam, n.publicatie_datum
FROM `nieuwsberichten` AS n
INNER JOIN schrijvers AS s
ON n.user_id = s.id
WHERE s.naam LIKE 'jij%';

## Opdracht 4
## Hoeveel berichten heeft de gebruiker me geschreven?

SELECT COUNT(*) AS 'Aantal Berichten'
FROM `nieuwsberichten` AS n
INNER JOIN schrijvers AS s
ON n.user_id = s.id
WHERE s.naam LIKE 'ik';

+------------------+
+ Aantal Berichten +
+------------------+
+ 2                +
+------------------+

