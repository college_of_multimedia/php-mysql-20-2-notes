<?php
/*
 * Haal de url op, bijvoorbeeld: http://www.deslager.test/mijn_mooie_nieuwsbericht
 * En verander deze in een array door de string bij elke slash uit elkaar te halen.
 */
$request_url = explode('/', $_SERVER['REDIRECT_URL']);
/*
 * Zoek nu het laatste onderdeel uit de array
 * Dit is de slug van dit nieuwsbericht
 */
$mijn_pagina = end($request_url);


echo 'ik bekijk nu het bericht: ' . $mijn_pagina;
