<?php
/*
 *
 * maak een nieuw bestandje in je editor
 * sla deze op in de folder waar je je nieuws pagina's hebt ( gebruik het absolute root path, bijvoorbeeld /sites/deslager/public_html/ )
 * noem dit bestand ".htaccess"
 * plaats de volgende code in dit bestand en sla het op:
---------------------- HTACCESS BEGIN --------------------------

# zet de engine aan
RewriteEngine On

# zeg wat er moet gebeuren
RewriteRule !(\.) nieuwsbericht.php [L]
---------------------- HTACCESS EINDE --------------------------

 * waarbij nieuwsbericht.php de pagina is waar je naartoe linkt
 * in deze pagina plaats je de onderstaande code om de url ( titel ) op te halen
*/


/*
 * Haal de url op, bijvoorbeeld: http://www.deslager.test/mijn_mooie_nieuwsbericht
 * En verander deze in een array door de string bij elke slash uit elkaar te halen.
 */
$request_url = explode('/', $_SERVER['REDIRECT_URL']);
/*
 * Zoek nu het laatste onderdeel uit de array
 * Dit is de slug van dit nieuwsbericht
 */
$mijn_pagina = end($request_url);


echo 'ik bekijk nu het bericht: ' . $mijn_pagina;