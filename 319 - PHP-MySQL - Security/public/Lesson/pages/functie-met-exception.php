<?php

//create function with an exception

/**
 * Controleer of een getal kleiner is dan 10
 *
 * @param $value
 *
 * @return bool
 * @throws \Exception
 */
function checkNumber($value)
{
    if ($value < 10) {
        throw new Exception('Vul iets kleiner dan 10 in');
    }

    return true;
}

/*
 * Hier even testen
 */
try {
    checkNumber(12);
    echo 'Het getal is kleiner dan 10';

} catch (Exception $exception) {
    echo 'Message: ' . $exception->getMessage();
}