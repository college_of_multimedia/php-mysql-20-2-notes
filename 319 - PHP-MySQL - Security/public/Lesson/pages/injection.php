<?php

if ( isset($_GET['name']) && isset($_GET['pass']) ) {
	$name = $_GET['name'];
	$pass = $_GET['pass'];

	require_once ('sanit.php');
	$name = sanit_input($name); ## make it secure!
	$pass = sanit_input($pass); ## make it secure!
	
	// Maak verbinding met een mysql server. Host, Username, Password, Database
	$mysqli_link = mysqli_init();
	$success = mysqli_real_connect( $mysqli_link, 'localhost', 'root', 'root', 'cmm_wd319_example' );
	if ( mysqli_connect_error() ) {
		die("Kan geen verbinding met de database maken");
	}

	// GET THE RIGHT USER==
	$query 	= "SELECT * FROM `users` WHERE username='$name' AND pass=md5('$pass')";
	echo "query: $query<br />\n";
	$mysqli_result = mysqli_query ( $mysqli_link, $query );
	if ( mysqli_error( $mysqli_link ) ) {
		echo mysqli_error( $mysqli_link );
		die( );
	}

	// FETCH DATA
	if ( mysqli_num_rows( $mysqli_result ) > 0 ) {
	
		// WE HAVE A RESULT, PRINT IT
		echo "JE BENT INGELOGD: <br />";
		$pageRes = mysqli_fetch_assoc($mysqli_result);	
		echo "<pre>";
		print_r($pageRes);
		echo "</pre>";
		echo "<br />----------------------------------------------------------------<br />\n";
		echo "<h1>{$pageRes['username']}</h1>";
	}
} else {
	$name = "";
	$pass = "";
}

?>

<br />
----------------------------------------------------------------<br />
<br />

<form action="" method="get" accept-charset="utf-8">
	user: <input type="text" name="name" value="<?php echo $name; ?>" /><br />
	pass: <input type="text" name="pass" value="<?php echo $pass; ?>" /><br />
	<input type="submit" value="login" />
</form>


<table border="1" cellspacing="1" cellpadding="5">
<tr>
	<th>URL</th>
	<th width="300">USAGE</th>
</tr>
<tr>
	<td><a href="<?=$_SERVER['PHP_SELF']?>"><?=$_SERVER['PHP_SELF']?></a></td>
	<td>Normaal</td>
</tr>
<tr>
	<td><a href="<?=$_SERVER['PHP_SELF']?>?name=%27+OR+1%3D1+%23&pass=blaat"><?=$_SERVER['PHP_SELF']?>?name=%27+OR+1%3D1+%23&pass=blaat</a></td>
	<td>Basis, <br />
	' OR 1=1 # <br />
	blaat	<br /></td>
</tr>
</table>