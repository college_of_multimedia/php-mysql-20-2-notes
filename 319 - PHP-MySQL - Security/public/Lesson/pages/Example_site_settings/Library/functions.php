<?php

/**
 * Debug functie
 *
 * Deze functie laat een outlined <pre> blok zien, 
 * met eventueel het bestandspad en regelnummer erbij
 * en dumpt dan de variable in het blok
 * 
 * @param mixed     $val    Variabele om to debuggen
 * @param string    $file   use __FILE__ voor bestandspad
 * @param string    $line   use __LINE__ voor regelnummer
 * 
 * @return void
 */
function debuggen($val, $file = '', $line = ''){
    global $debug;
    if ($debug){
        echo '<pre style="border: solid black 1px">';
        if ('' !== $file) {
            echo "$file \nline $line \n";
        }
        var_dump($val);
        echo '</pre>';
    }
}

/**
 * Active Menu function
 *
 * @param  string   $test   bijvoorbeeld home, about, contact 
 * @return string			lege string of de active class
 */
function activeMenu($test){
    global $active;
    if ($active == $test){
        return 'class="active"';
    }
    return '';
}
