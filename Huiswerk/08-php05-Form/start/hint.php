<?php
## Voorbeeld van een Array omzeten naar options van een select formulier veld

/*
 * Defineer een kleuren array met alleen values
 * Dit is dus een Indexed array
 */
$kleuren = ['rood', 'geel', 'paars', 'roze'];
?>
<select name="kleur">
	<option value=""> - maak uw keuze -</option>
	<?php
	/*
	 * Loop door de kleuren array heen
	 * ik haal alleen de values op omdat de keys nu niet interessant zijn
	 */
	foreach ($kleuren as $kleur) {
		/*
		 * Geef elke optie een waarde
		 */
		echo '<option value="' . $kleur . '"';
		/*
		 * Als de kleur geselecteerd is dan wil ik deze weer selecteren
		 * hardcoded voorbeeld: roze
		 */
		if ('roze' == $kleur) {
			echo ' selected';
		}
		/*
		 * Sluit de optie af
		 */
		echo ' > ' . ucfirst($kleur) . '</option>' . "\n";
	}
	?>
</select><br>