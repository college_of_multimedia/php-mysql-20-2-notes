<?php

$my_var = 1;
print_r( $my_var );

$my_var = 'hoi hoi';
print_r( $my_var );

$my_var = array( 'een', 'twee' );
print_r( $my_var );


$my_var = 1;
var_dump( $my_var );

$my_var = 'hoi hoi';
var_dump( $my_var );

$my_var = array( 'een', 'twee' );
var_dump( $my_var );
