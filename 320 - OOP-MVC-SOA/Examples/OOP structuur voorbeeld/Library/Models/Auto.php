<?php

class Auto
{
    protected $_wielen = 4;
    protected $_kleur = 'rood';

    public function __construct($wielen = 2, $kleur = 'geel')
    {
        $this->_wielen = $wielen;
    }


    public function getKleur()
    {
        return $this->_kleur;
    }

}
