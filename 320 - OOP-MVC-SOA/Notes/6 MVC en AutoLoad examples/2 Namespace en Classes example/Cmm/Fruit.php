<?php

namespace Cmm;

class Fruit 
{
	/**
	 *  @return string
	 */
	public function showSomeFruit()
	{
		$tekst = 'Hi, hier is wat fruit';
		$appel = new Appel();
		$tekst .= $appel->show();

		$peer = new Peer();
		$tekst .= $peer->show();

		return $tekst;	
	}

}