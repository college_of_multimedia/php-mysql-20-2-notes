<?php

spl_autoload_register(
	function ($class)
	{
		if (class_exists($class)) {
			return;
		}	
		$filepath = str_replace('\\', '/', $class). '.php';
		if (file_exists(__DIR__ . '/' . $filepath)) {
			require_once(__DIR__ . '/' . $filepath);
		}
	}
);

use \Cmm\Fruit;
use \Cmm\Peer;

$fruit = new Fruit();
echo $fruit->showSomeFruit();
echo "<br>\n";

$peer = new Peer();
echo '<b>nog meer</b>:' .  $peer->titel();
