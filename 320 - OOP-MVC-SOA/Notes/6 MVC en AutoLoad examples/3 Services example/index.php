<?php

/**
 * AutoLoad Service
 *
 * Met deze functie kun je onderdelen op basis van een NameSpacing ophalen
 */
spl_autoload_register(
    function ($class)
    {
        if (class_exists($class)) {
            return;
        }
        $filepath = str_replace('\\', '/', $class).'.php';

        if (file_exists(__DIR__.'/'.$filepath)) {
            require_once(__DIR__.'/'.$filepath);
        }
    }
);


define('DOCUMENT_ROOT', __dir__);

// $pdo = new PDO(':host=localhost;dbname=your_db;', 'root', 'root');

$pdo ='test';

## autoload met pad
/*
$newsController = new News\Controller($pdo);

$newsController->showNewsItem(1);

echo '<hr>';

$newsController->showNewsItems();
*/



## autoload met use

use News\Controller;

$newsController = new Controller($pdo);

$newsController->showNewsItem(2);

echo '<hr>';
echo '<hr>';

$newsController->showNewsItems();

