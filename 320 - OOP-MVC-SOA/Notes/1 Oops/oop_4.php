<?php
$debug = true;

ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$nl = "\n";
$br = '<br>' . $nl;

echo '<h2>OOP\'s part  4 </h2>';

require_once 'ProtectedUser.php';
require_once 'ExtendedUser.php';

$user1 = new ProtectedUser('Bert');
$user2 = new ExtendedUser('Tom', 12);

echo $user1->getUsername() . $br;
echo $user2->getUsername() . $br;

$user1->setUsername('Bertus');
$user2->setUsername('Thomas');

echo $user1->getUsername() . $br;
echo $user2->getUsername() . $br;


// echo $user2->_username . $br; ## mag niet is protected of private


