<?php
$debug = true;

ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$nl = "\n";
$br = '<br>' . $nl;

echo '<h2>OOP\'s part 2 </h2>';

class User {

	/**
	 * @var string
	 */
	protected $_username;

	/**
	 * User constructor
	 * @param string $name 
	 */
	public function __construct( $name )
	{
		$this->setUsername( $name );
	}

	/**
	 * set the username 
	 * @param string $name 
	 */
	public function setUsername ( $name ) {
		$this->_username = $name;
	} 

	/**
	 * get the username 
	 * @return string 
	 */
	public function getUsername () {
		return $this->_username;
	} 
}



$user1 = new User('Bert');
$user2 = new User('Truus');
$user3 = new User('Klaas');

echo $user1->getUsername() 
	. ' en ' .  $user2->getUsername() 
	. ' en ' .  $user3->getUsername()  
	. $br;








