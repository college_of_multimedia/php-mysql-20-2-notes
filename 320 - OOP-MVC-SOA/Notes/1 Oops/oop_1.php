<?php
$debug = true;

ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$nl = "\n";
$br = '<br>' . $nl;

echo '<h2>OOP\'s 1 </h2>';

class User {

	/**
	 * @var string
	 */
	protected $_username;

	/**
	 * set the username 
	 * @param string $name 
	 */
	public function setUsername ( $name ) {
		$this->_username = $name;
	} 

	/**
	 * get the username 
	 * @return string 
	 */
	public function getUsername () {
		return $this->_username;
	} 
}



$user1 = new User();
$user1->setUsername( 'Bert' );
// echo $user1->getUsername() . $br;

$user2 = new User();
$user2->setUsername( 'Truus' );

echo $user1->getUsername() . ' en ' .  $user2->getUsername() . $br;

// Mag niet want is protected!
$user1->_username = "Harald";
echo $user1->_username;







